import './App.css';
import React, { useState } from 'react';
import Login from './components/login';
import Home from './components/home';
import Navigation from './components/navigation';
import Consult from './components/consult';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Register } from './components/register';
import SingUp from './components/sing-up';

function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(true);

  const cerrarSesion = () => {
    console.log(isLoggedIn);
    setIsLoggedIn(false);
    window.location = "/login"
  }

  return (
    <div className="App">
      {isLoggedIn && <Navigation cerrarSesion={cerrarSesion} />} {}
      <div className='container'>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/sing-up" element={<SingUp />} />
            <Route path="/register" element={<Register />} />
            <Route path="/consult" element={<Consult />} />
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
