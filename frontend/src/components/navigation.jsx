import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

function Navigation({ cerrarSesion }) {

    let go_to_home = () => {
        window.location = "/"
    }

    let go_to_register = () => {
        window.location = "/register"
    }

    let go_to_consult = () => {
        window.location = "/consult"
    }

    return (
        <Navbar expand="lg" className="bg-body-tertiary">
            <Container>
                <Navbar.Brand onClick={go_to_home}>Actividades</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link onClick={go_to_register}>Registrar</Nav.Link>
                        <Nav.Link onClick={go_to_consult}>Consultar</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        <a href="#login" onClick={cerrarSesion}>Salir</a>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Navigation;