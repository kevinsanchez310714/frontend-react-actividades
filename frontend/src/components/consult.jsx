import React from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';


function Consult() {
    return (
        <div>
            <h1 className="py-4">Consultar actividad</h1>
            <Form>
                <Row>
                    <Col>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Estado</Form.Label>
                            <Form.Control type="date" />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Estado</Form.Label>
                            <Form.Select aria-label="Default select example">
                                <option>-- Seleccione --</option>
                                <option value="1">Completada</option>
                                <option value="2">Pendiente</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
                <div className="text-center">
                    <Button variant="primary" type="submit">
                        Consultar
                    </Button>
                </div>
            </Form>
            <Table responsive="sm" className="mt-5">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Actividad</th>
                        <th>Descripción</th>
                        <th>Tiempo</th>
                        <th>Fecha</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Login</td>
                        <td>Se realiza interfaz para el inicio de sesion</td>
                        <td>1 Hora</td>
                        <td>2024-01-01</td>
                        <td>Pendiente</td>
                        <td>
                            <a href="#">Editar</a> - <a href="#">Borrar</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Registrar</td>
                        <td>Se realiza interfaz para el registro del usuario</td>
                        <td>2 Horas</td>
                        <td>2024-01-08</td>
                        <td>Pendiente</td>
                        <td>
                            <a href="#">Editar</a> - <a href="#">Borrar</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>tareas</td>
                        <td>Opcion para registrar la tarea</td>
                        <td>3 Horas</td>
                        <td>2024-01-01</td>
                        <td>Completado</td>
                        <td>
                            <a href="#">Editar</a> - <a href="#">Borrar</a>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </div>
    )
}

export default Consult;
