import React from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

class Register extends React.Component {
    render() {
        return (
            <div>
                <h1 className="py-4">Registrar actividad</h1>
                <Form action="/register_activity" method="post">
                    <Row>
                        <Col>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Actividad</Form.Label>
                                <Form.Control type="text" name="actividad" id="actividad" />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Tiempo</Form.Label>
                                <Form.Control type="text" name="time" id="time" />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Estado</Form.Label>
                                <Form.Select aria-label="Default select example" name="state" id="state">
                                    <option>-- Seleccione --</option>
                                    <option value="1">Completada</option>
                                    <option value="2">Pendiente</option>
                                </Form.Select>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Descripción</Form.Label>
                        <Form.Control as="textarea" rows={3} name="description" id="description"/>
                    </Form.Group>
                    <div className="text-center">
                        <Button variant="primary" type="submit" id="btn-submit">
                            Guardar
                        </Button>
                    </div>
                </Form>
            </div>
        );
    }

}

export {
    Register
}