import ex from 'express';
import dir from 'path'

import './db.js'
import insert_record from './db.js';

const app = ex();
const dir_front = dir.resolve().replace("server", "");

app.use(ex.static("../frontend/build"))
app.use(ex.json())
app.use(ex.urlencoded({}))

app.get('/*', (rq, res) => {
    res.sendFile(dir_front + "frontend/build/index.html")
})

app.post("/register_activity", (req, res) => {
    let { actividad, time, state, description } = req.body;
    insert_record(actividad, time, state, description)
})
// app.get('/register', (rq, res) => {
//     res.sendFile(dir_front + "frontend/build/index.html")
// })

// app.get('/consult', (rq, res) => {
//     res.sendFile(dir_front + "frontend/build/index.html")
// })


app.listen('5500', () => {
    console.log('Server runing in port 5500')
});