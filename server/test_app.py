from selenium import webdriver;
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
url = "http://localhost:5500"
driver.get(url)
time.sleep(2)

def test_register():
    btn_register_page = driver.find_element(By.XPATH, '//*[@id="basic-navbar-nav"]/div/a[1]')
    btn_register_page.click()
    time.sleep(2)
    
    activity_field = driver.find_element(By.XPATH, '//*[@id="actividad"]')
    activity_field.send_keys("Comer pollo")
    time.sleep(1)

    time_field = driver.find_element(By.XPATH, '//*[@id="time"]')
    time_field.send_keys("20")
    time.sleep(1)

    estate_field = driver.find_element(By.XPATH, '//*[@id="state"]')
    estate_field.send_keys("Completada")
    time.sleep(1)

    description_field = driver.find_element(By.XPATH, '//*[@id="description"]')
    description_field.send_keys("Todas las mañanas al salir el sol")
    time.sleep(1)

    btn_register_user = driver.find_element(By.XPATH, '//*[@id="btn-submit"]')
    btn_register_user.click()
    time.sleep(1)